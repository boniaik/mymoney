package com.test.mymoney.dto;

import com.test.mymoney.util.AssetType;

public class Change {
    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public AssetType getType() {
        return type;
    }

    public void setType(AssetType type) {
        this.type = type;
    }

    private double percentage;
    private AssetType type;

}
