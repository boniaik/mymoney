package com.test.mymoney.dto;

import java.util.List;

public class PortfolioLineItem {

    private String operation;
    private List<AssetValue> assetValues;
    private Long total;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public List<AssetValue> getAssetValues() {
        return assetValues;
    }

    public void setAssetValues(List<AssetValue> assetValues) {
        this.assetValues = assetValues;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}

