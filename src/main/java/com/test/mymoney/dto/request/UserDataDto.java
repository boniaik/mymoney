package com.test.mymoney.dto.request;

public class UserDataDto {

    /**
     * userId : unique identifier for an individual user
     * filePath : path of the file which contains user data
     */
    private long userId;
    private String filePath;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
