package com.test.mymoney.dto;

import java.time.Month;

public class BalanceDto {
    private Month month;

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }
}
