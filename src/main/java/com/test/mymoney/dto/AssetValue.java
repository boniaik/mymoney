package com.test.mymoney.dto;

import com.test.mymoney.util.AssetType;

public class AssetValue {

    private AssetType type;

    private Long value;

    public AssetType getType() {
        return type;
    }

    public void setType(AssetType type) {
        this.type = type;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
