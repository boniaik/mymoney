package com.test.mymoney.dto;

import java.util.List;

public class AllocateDto {

    private List<Long> allocations;

    public List<Long> getAllocations() {
        return allocations;
    }

    public void setAllocations(List<Long> allocations) {
        this.allocations = allocations;
    }
}
