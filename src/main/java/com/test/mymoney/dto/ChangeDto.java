package com.test.mymoney.dto;

import com.test.mymoney.util.AssetType;

import java.time.Month;
import java.util.List;

public class ChangeDto {

    private List<Change> changes;

    private Month month;

    public List<Change> getChanges() {
        return changes;
    }

    public void setChanges(List<Change> changes) {
        this.changes = changes;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }
}

