package com.test.mymoney.dto;

import java.util.List;

public class PortfolioStore {

    private List<PortfolioLineItem> lineItems;

    public List<PortfolioLineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<PortfolioLineItem> lineItems) {
        this.lineItems = lineItems;
    }
}
