package com.test.mymoney.repository;

import com.test.mymoney.util.AssetType;
import org.springframework.stereotype.Component;

import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class UserPortfolioDataRepo {

    private ConcurrentMap<Long, Map<AssetType, Long>> userAssetMap = new ConcurrentHashMap<>();
    private ConcurrentMap<Long, Map<AssetType, Long>> userSIPMap = new ConcurrentHashMap<>();
    private ConcurrentMap<Long, Map<Month, Map<AssetType, Double>>> userChangeListMap = new ConcurrentHashMap<>();

    public boolean addAllocations(Long userId, AssetType type, Long value){
        if(userAssetMap.containsKey(userId)){
            userAssetMap.get(userId).put(type, value);
            return true;
        }
        Map<AssetType, Long> assetMap = new HashMap<>();
        assetMap.put(type, value);
        userAssetMap.put(userId, assetMap);
        return true;
    }

    public boolean addSIP(Long userId, AssetType type, Long value){
        if(userSIPMap.containsKey(userId)){
            userSIPMap.get(userId).put(type, value);
            return true;
        }
        Map<AssetType, Long> sipMap = new HashMap<>();
        sipMap.put(type, value);
        userSIPMap.put(userId, sipMap);
        return true;
    }

    public boolean addChange(Long userId, Month month, AssetType type, Double value){
        if(userChangeListMap.containsKey(userId)){
            if(userChangeListMap.get(userId).containsKey(month)){
                userChangeListMap.get(userId).get(month).put(type, value);
                return true;
            }
            Map<AssetType, Double> assetMap = new HashMap<>();
            assetMap.put(type, value);
            userChangeListMap.get(userId).put(month, assetMap);
            return true;
        }
        Map<Month, Map<AssetType, Double>> monthChangeMap = new HashMap<>();
        Map<AssetType, Double> assetMap = new HashMap<>();
        assetMap.put(type, value);
        monthChangeMap.put(month, assetMap);
        userChangeListMap.put(userId, monthChangeMap);
        return true;
    }

    public Long getAssetAllocation(Long userId, AssetType type){
        if(userAssetMap.containsKey(userId)){
            return userAssetMap.get(userId).get(type);
        }
        return 0L;
    }

    public Map<AssetType, Long> getAssetAllocationMap(Long userId){
        if(userAssetMap.containsKey(userId)){
            return userAssetMap.get(userId);
        }
        return null;
    }

    public Long getSIPValue(Long userId, AssetType type){
        if(userSIPMap.containsKey(userId)){
            return userSIPMap.get(userId).get(type);
        }
        return 0L;
    }

    public Map<AssetType, Long> getSIPValueMap(Long userId){
        if(userSIPMap.containsKey(userId)){
            return userSIPMap.get(userId);
        }
        return null;
    }

    public Double getChangeValue(Long userId, Month month, AssetType type){
        if(userChangeListMap.containsKey(userId) && userChangeListMap.get(userId).containsKey(month)){
            return userChangeListMap.get(userId).get(month).get(type);
        }
        return 0.0;
    }

    public Month getMaxMonth(Long userId){
        if(userChangeListMap.containsKey(userId)){
           Set<Month> monthSet = userChangeListMap.get(userId).keySet();
           Object[] sortedMonths = monthSet.stream().sorted().toArray();
           return (Month) sortedMonths[sortedMonths.length-1];
        }
        return Month.JANUARY;
    }
}
