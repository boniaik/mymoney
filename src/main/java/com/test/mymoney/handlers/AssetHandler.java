package com.test.mymoney.handlers;

import com.test.mymoney.util.AssetType;

import java.time.Month;

public interface AssetHandler {

    Long balance(Long userId, Month m);

    Long balance(Long userId, Long prevBalance, Month fromMonth, Month toMonth);

    void rebalance(long total, Month m);

    AssetType getType();
}
