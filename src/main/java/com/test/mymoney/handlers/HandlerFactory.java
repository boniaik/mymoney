package com.test.mymoney.handlers;

import com.test.mymoney.util.AssetType;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class HandlerFactory {

    private List<AssetHandler> handlers;

    public HandlerFactory(List<AssetHandler> handlers){
        this.handlers = handlers;
    }

//    void init(){
//        // create the specific handler objects and add to the list
//        handlers.add(new EquityHandler());
//        handlers.add(new DebtHandler());
//        handlers.add(new GoldHandler());
//    }

    public AssetHandler getHandler(AssetType type) throws Exception {

        for(AssetHandler ah: handlers){
            if(ah.getType().equals(type)){
                return ah;
            }
        }
        throw new Exception("No handler found");
    }
}
