package com.test.mymoney.handlers;

import com.test.mymoney.repository.UserPortfolioDataRepo;
import com.test.mymoney.util.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Month;

@Component
public class EquityHandler implements AssetHandler {

    private AssetType type = AssetType.Equity;

    private UserPortfolioDataRepo userPortfolioDataRepo;

    public EquityHandler(UserPortfolioDataRepo userPortfolioDataRepo){
        this.userPortfolioDataRepo = userPortfolioDataRepo;
    }
    @Override
    public Long balance(Long userId, Month m) {
        Long initialAllocation = userPortfolioDataRepo.getAssetAllocation(userId, type);
        Long sipValue = userPortfolioDataRepo.getSIPValue(userId, type);

        double balance = initialAllocation;

        for(Month month : Month.values()){
            if(month.getValue() > m.getValue()){
                break;
            }
            double change = userPortfolioDataRepo.getChangeValue(userId, month, type);
            balance = month == Month.JANUARY ? balance + ((balance * change)/100)
                    : balance + (((balance + sipValue) * change)/100) + sipValue;

        }
        return (long) balance;
    }

    /**
     *
     * @param userId
     * @param prevBalance balance after 1st re-balance
     * @param fromMonth should be after june
     * @param toMonth till dec
     * @return
     */

    public Long balance(Long userId, Long prevBalance, Month fromMonth, Month toMonth){
        Long sipValue = userPortfolioDataRepo.getSIPValue(userId, type);

        double balance = prevBalance;

        for(Month month : Month.values()){
            if(month.getValue() < fromMonth.getValue()){
                continue;
            }
            if(month.getValue() > toMonth.getValue()){
                break;
            }
            double change = userPortfolioDataRepo.getChangeValue(userId, month, type);
            balance = balance + (((balance + sipValue) * change)/100) + sipValue;

        }
        return (long) balance;
    }

    @Override
    public void rebalance(long total, Month m) {

    }

    @Override
    public AssetType getType() {
        return type;
    }
}
