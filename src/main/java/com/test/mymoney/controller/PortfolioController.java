package com.test.mymoney.controller;

import com.test.mymoney.dto.response.ResponseWrapper;
import com.test.mymoney.service.PortfolioService;
import com.test.mymoney.util.AssetType;
import org.springframework.web.bind.annotation.*;

import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("user/portfolio")
public class PortfolioController {

    private PortfolioService portfolioService;

    public PortfolioController(PortfolioService portfolioService){
        this.portfolioService = portfolioService;
    }

    @GetMapping("/{userId}/{month}")
    public ResponseWrapper<Map<AssetType, Long>> getBalance(@PathVariable("userId") long userId, @PathVariable("month") Month month){

        try{
            Map<AssetType, Long> balances = portfolioService.getBalance(userId, month);
            return new ResponseWrapper(balances, "Successfully user data loaded", true);
        }catch (Exception e){
            // print error logs
            System.out.println("Error : "+ e.getStackTrace().toString());
        }

        return new ResponseWrapper(null,"Failed to get balance", false );
    }

    @PostMapping("/rebalance/{userId}")
    public ResponseWrapper<Map<AssetType, Long>> performRebalanced(@PathVariable("userId") long userId){
        try{
            Map<AssetType, Long> balances = portfolioService.performRebalanced(userId);
            return new ResponseWrapper(balances, "Successfully user data loaded", true);
        }catch (Exception e){
            // print error logs
            System.out.println("Error : "+ e.getStackTrace().toString());
        }

        return new ResponseWrapper(false, "Failed to balance user data", false);
    }

}
