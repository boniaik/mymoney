package com.test.mymoney.controller;

import com.test.mymoney.dto.response.ResponseWrapper;
import com.test.mymoney.dto.request.UserDataDto;
import com.test.mymoney.service.AdminService;
import org.springframework.web.bind.annotation.*;

import java.time.Month;

@RestController
@RequestMapping("admin")
public class AdminController {

    private AdminService adminService;

    public AdminController(AdminService adminService){
        this.adminService = adminService;
    }


    @PostMapping("/user-data")
    public ResponseWrapper<Boolean> loadUserData(@RequestBody UserDataDto userData){

        try{
            adminService.loadUserData(userData);
            return new ResponseWrapper(true, "Successfully user data loaded", true);
        }catch (Exception e){
            // print error logs
            System.out.println("Error : "+ e.getMessage());
        }

        return new ResponseWrapper(false, "Failed to load user data", false);
    }
}
