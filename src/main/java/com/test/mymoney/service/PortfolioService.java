package com.test.mymoney.service;

import com.test.mymoney.exception.CanNotRebalanceException;
import com.test.mymoney.handlers.HandlerFactory;
import com.test.mymoney.repository.UserPortfolioDataRepo;
import com.test.mymoney.util.AssetType;
import org.springframework.stereotype.Service;

import java.time.Month;
import java.util.*;

@Service
public class PortfolioService {

    private HandlerFactory factory;
    private UserPortfolioDataRepo userPortfolioDataRepo;

    public PortfolioService(HandlerFactory factory, UserPortfolioDataRepo userPortfolioDataRepo) {
        this.factory = factory;
        this.userPortfolioDataRepo = userPortfolioDataRepo;
    }


    public Map<AssetType, Long> getBalance(Long userId, Month month) throws Exception {
        Map<AssetType, Long> balances = new HashMap<AssetType, Long>();
        for(AssetType type : AssetType.values()){
            balances.put(type, factory.getHandler(type).balance(userId, month));
        }
        return balances;
    }

    private Map<AssetType, Long> getBalance(Long userId, Map<AssetType, Long> prevBalMap, Month fromMonth, Month month) throws Exception {
        Map<AssetType, Long> balances = new HashMap<AssetType, Long>();
        for(AssetType type : AssetType.values()){
            balances.put(type, factory.getHandler(type).balance(userId, prevBalMap.get(type), fromMonth, month));
        }
        return balances;
    }

    public Map<AssetType, Long> performRebalanced(Long userId) throws Exception {

        Month maxMonth = userPortfolioDataRepo.getMaxMonth(userId);

        if(maxMonth.getValue() < Month.JUNE.getValue()){
            throw new CanNotRebalanceException("CANNOT_REBALANCE");
        }

        Map<AssetType, Long> resultMap = new HashMap<AssetType, Long>();
        Map<AssetType, Long> assetMap = userPortfolioDataRepo.getAssetAllocationMap(userId);
        long initialTotal = getTotal(assetMap.values());
        Map<AssetType, Long> juneBalanceMap = getBalance(userId, Month.JUNE);
        Collection<Long> balances = juneBalanceMap.values();
        long juneTotal = getTotal(balances);
        if(maxMonth.getValue() < Month.DECEMBER.getValue()){
            return getRebalance(assetMap, juneTotal, initialTotal);
        }
        Map<AssetType, Long> juneResultMap = getRebalance(assetMap, juneTotal, initialTotal);
        Map<AssetType, Long> decBalanceMap = getBalance(userId, juneResultMap, Month.JUNE, maxMonth);
        long decTotal = getTotal(decBalanceMap.values());
        return getRebalance(assetMap, decTotal, initialTotal);
    }

    private long getTotal(Collection<Long> balances) {
        long total = 0;

        for(long l : balances){
            total = total + l;
        }
        return total;
    }

    private Map<AssetType, Long> getRebalance(Map<AssetType, Long> assetMap, Long juneTotal, Long initialTotal){
        Map<AssetType, Long> resultMap = new HashMap<AssetType, Long>();
        for(AssetType type : assetMap.keySet()){
            long assetInitialValue = assetMap.get(type);
            long rebalancedValue = (juneTotal * assetInitialValue)/initialTotal;
            resultMap.put(type, rebalancedValue);
        }
        return resultMap;
    }

}
