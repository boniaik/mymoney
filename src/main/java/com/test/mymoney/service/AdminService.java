package com.test.mymoney.service;

import com.test.mymoney.dto.request.UserDataDto;
import com.test.mymoney.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    private Utility utility;

    public AdminService(Utility utility){
        this.utility = utility;
    }

    public boolean loadUserData(UserDataDto userData){
        return this.utility.loadInput(userData.getUserId(), userData.getFilePath()) != null;
    }

}
