package com.test.mymoney.exception;

public class CanNotRebalanceException extends Exception {
    public CanNotRebalanceException(String msg){
        super(msg);
    }
}
