package com.test.mymoney.util;

public enum AssetType {

    Equity,
    Debt,
    Gold
}
