package com.test.mymoney.util;

public enum Commands{

    ALLOCATE("ALLOCATE"),
    SIP("SIP"),
    CHANGE("CHANGE"),
    BALANCE("BALANCE"),
    REBALANCE("REBALANCE");

    private String value;

    Commands(String v){
        this.value = v;
    }

    public String getStringValue() {
        return value;
    }
}
