package com.test.mymoney.util;

import com.test.mymoney.dto.PortfolioLineItem;
import com.test.mymoney.dto.PortfolioStore;
import com.test.mymoney.repository.UserPortfolioDataRepo;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.Month;
import java.util.LinkedList;
import java.util.List;

// kirankumardg@zscaler.com

@Component
public class Utility {

    private UserPortfolioDataRepo userPortfolioDataRepo;

    public Utility(UserPortfolioDataRepo userPortfolioDataRepo){
        this.userPortfolioDataRepo = userPortfolioDataRepo;
    }

    public PortfolioStore loadInput(Long userId, String path){

        PortfolioStore ps = new PortfolioStore();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            //PortfolioLineItem pre
            while((line = br.readLine()) != null){ //
                String[] tokens = line.split(" ");

                switch (Commands.valueOf(tokens[0])) {
                    case ALLOCATE:
                        allocate(userId, tokens);
                        break;
                    case SIP:
                        sip(userId, tokens);
                        break;
                    case CHANGE:
                        change(userId, tokens);
                        break;
                    case BALANCE:
                        balance(tokens[1]);
                        break;
                    case REBALANCE:
                        rebalanced();
                        break;
                    default:
                        break;
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ps;
    }

    private boolean allocate(Long userId, String[] s){

//        List<AssetValue> assetValueList = new LinkedList<AssetValue>();
//        pi.setOperation("January - "+ Commands.ALLOCATE);
//        AssetValue qa = new AssetValue();
//        qa.setType(AssetType.Equity);
//        qa.setValue(Long.parseLong(s[1]));
//        assetValueList.add(qa);
//        AssetValue da = new AssetValue();
//        da.setType(AssetType.Debt);
//        da.setValue(Long.parseLong(s[2]));
//        assetValueList.add(da);
//        AssetValue ga = new AssetValue();
//        ga.setType(AssetType.Gold);
//        ga.setValue(Long.parseLong(s[3]));
//        assetValueList.add(da);
//        pi.setAssetValues(assetValueList);
//        pi.setTotal(qa.getValue() + da.getValue() + ga.getValue());

        Long equityAllocation = Long.parseLong(s[1]);
        Long debtAllocation = Long.parseLong(s[2]);
        Long goldAllocation = Long.parseLong(s[3]);

        userPortfolioDataRepo.addAllocations(userId, AssetType.Equity, equityAllocation);
        userPortfolioDataRepo.addAllocations(userId, AssetType.Debt, debtAllocation);
        userPortfolioDataRepo.addAllocations(userId, AssetType.Gold, goldAllocation);
        return true;
    }

    private boolean sip(Long userId, String[] tokens){
        Long equitySIP = Long.parseLong(tokens[1]);
        Long debtSIP = Long.parseLong(tokens[2]);
        Long goldSIP = Long.parseLong(tokens[3]);

        userPortfolioDataRepo.addSIP(userId, AssetType.Equity, equitySIP);
        userPortfolioDataRepo.addSIP(userId, AssetType.Debt, debtSIP);
        userPortfolioDataRepo.addSIP(userId, AssetType.Gold, goldSIP);
        return true;
    }

    private boolean change(Long userId, String[] tokens){
        Double equityChange = Double.parseDouble(tokens[1].replace("%", ""));
        Double debtChange = Double.parseDouble(tokens[2].replace("%", ""));
        Double goldChange = Double.parseDouble(tokens[3].replace("%", ""));

        Month month = Month.valueOf(tokens[4].toUpperCase());

        userPortfolioDataRepo.addChange(userId, month, AssetType.Equity, equityChange);
        userPortfolioDataRepo.addChange(userId, month, AssetType.Debt, debtChange);
        userPortfolioDataRepo.addChange(userId, month, AssetType.Gold, goldChange);
        return true;
    }

    private boolean balance(String month){

        return true;
    }

    private boolean rebalanced(){

        return true;
    }
}
